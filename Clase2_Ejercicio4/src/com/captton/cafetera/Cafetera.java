package com.captton.cafetera;

public class Cafetera {

	private int _capacidadMaxima;
	private int _cantidadActual;
	
	
	public Cafetera() {
		this._capacidadMaxima = 1000;
		this._cantidadActual = 0;
	}
	
	public Cafetera(int _cantidadActual) {
		this._capacidadMaxima = _cantidadActual;
	}
	
	public Cafetera(int _capacidadMaxima, int _cantidadActual) {
			this._capacidadMaxima = _cantidadActual;
			this._cantidadActual = _cantidadActual;
	}

	public int get_capacidadMaxima() {
		return _capacidadMaxima;
	}

	public void set_capacidadMaxima(int _capacidadMaxima) {
		this._capacidadMaxima = _capacidadMaxima;
	}

	public int get_cantidadActual() {
		return _cantidadActual;
	}

	public void set_cantidadActual(int _cantidadActual) {
		this._cantidadActual = _cantidadActual;
	}
	
	
	
	public void llenarCafe() {
		this._cantidadActual = this._capacidadMaxima;
	}
	
	public int servirTaza(int capacidadTaza) {
		
		int taza=capacidadTaza;
		
		if(this._cantidadActual==0) {
			System.out.println("No hay mas cafe para servir");
		} else {
			
	
		if(this._cantidadActual > capacidadTaza) {
			this._cantidadActual -= capacidadTaza;
		} else {
			if(this._cantidadActual < capacidadTaza) {
				taza = this._cantidadActual;
				this._cantidadActual-=taza;
			}
		}
		
		if(taza==0) {
			System.out.println("No hay para servir");
		}
		else {
			System.out.println("Taza tiene: "+taza);
		}
	}
		return taza;
	}
	
	public void vaciarCafetera() {
		this._cantidadActual = 0;
	}
	
	public void agregarCafe(int cafe) {
		
		if((this._cantidadActual == 0)||(this._cantidadActual+cafe<=this._capacidadMaxima))
			this._cantidadActual+=cafe;
		else {
			System.out.println("No se pudo llenar");
		}
		
	}
		
}
