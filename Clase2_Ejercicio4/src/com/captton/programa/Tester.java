package com.captton.programa;

import com.captton.cafetera.Cafetera;

public class Tester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Cafetera cafetera = new Cafetera ();
		
		System.out.println("Llena cafe");
		cafetera.llenarCafe();
		System.out.println("Cantidad de cafe actual: "+cafetera.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Agrega cafe");
		cafetera.agregarCafe(500);
		System.out.println("Cantidad de cafe actual: "+cafetera.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Servir taza");
		cafetera.servirTaza(300);
		System.out.println("Cantidad de cafe actual: " + cafetera.get_cantidadActual());
		cafetera.servirTaza(250);
		System.out.println("Cantidad de cafe actual: " + cafetera.get_cantidadActual());
		cafetera.servirTaza(500);
		System.out.println("Cantidad de cafe actual: " + cafetera.get_cantidadActual());
		cafetera.servirTaza(350);
	
		
		System.out.println("////////");
		
		System.out.println("Agrega 500 de cafe");
		cafetera.agregarCafe(500);
		System.out.println("Cantidad de cafe actual: " + cafetera.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Agrega cafe");
		cafetera.agregarCafe(250);
		System.out.println("Cantidad de cafe actual: " + cafetera.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Agrega cafe");
		cafetera.agregarCafe(251);
		System.out.println("Cantidad de cafe actual: " + cafetera.get_cantidadActual());
		
		System.out.println("////////");
		System.out.println("Vacia cafetera");
		cafetera.vaciarCafetera();
		System.out.println(cafetera.get_cantidadActual());
		
		
		System.out.println("-------------------------------");
		Cafetera cafeteraConMaxima = new Cafetera (1500);
		
		System.out.println("Llena cafe");
		cafeteraConMaxima.llenarCafe();
		System.out.println("Cantidad de cafe actual: "+cafeteraConMaxima.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Agrega cafe");
		cafeteraConMaxima.agregarCafe(500);
		System.out.println("Cantidad de cafe actual: "+cafeteraConMaxima.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Servir taza");
		cafeteraConMaxima.servirTaza(300);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaxima.get_cantidadActual());
		cafeteraConMaxima.servirTaza(250);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaxima.get_cantidadActual());
		cafeteraConMaxima.servirTaza(500);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaxima.get_cantidadActual());
		cafeteraConMaxima.servirTaza(350);
	
		
		System.out.println("////////");
		
		System.out.println("Agrega 500 de cafe");
		cafeteraConMaxima.agregarCafe(500);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaxima.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Agrega cafe");
		cafeteraConMaxima.agregarCafe(250);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaxima.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Agrega cafe");
		cafeteraConMaxima.agregarCafe(251);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaxima.get_cantidadActual());
		
		System.out.println("////////");
		System.out.println("Vacia cafetera");
		cafeteraConMaxima.vaciarCafetera();
		System.out.println(cafeteraConMaxima.get_cantidadActual());
		
		System.out.println("-----------------------");
		Cafetera cafeteraConMaximayActual = new Cafetera (1000, 500);
		
		System.out.println("Llena cafe");
		cafeteraConMaximayActual.llenarCafe();
		System.out.println("Cantidad de cafe actual: "+cafeteraConMaximayActual.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Agrega cafe");
		cafeteraConMaximayActual.agregarCafe(500);
		System.out.println("Cantidad de cafe actual: "+cafeteraConMaximayActual.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Servir taza");
		cafeteraConMaximayActual.servirTaza(300);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaximayActual.get_cantidadActual());
		cafeteraConMaximayActual.servirTaza(250);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaximayActual.get_cantidadActual());
		cafeteraConMaximayActual.servirTaza(500);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaximayActual.get_cantidadActual());
		cafeteraConMaximayActual.servirTaza(350);
	
		
		System.out.println("////////");
		
		System.out.println("Agrega 500 de cafe");
		cafeteraConMaximayActual.agregarCafe(500);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaximayActual.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Agrega cafe");
		cafeteraConMaximayActual.agregarCafe(250);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaximayActual.get_cantidadActual());
		
		System.out.println("////////");
		
		System.out.println("Agrega cafe");
		cafeteraConMaximayActual.agregarCafe(251);
		System.out.println("Cantidad de cafe actual: " + cafeteraConMaximayActual.get_cantidadActual());
		
		System.out.println("////////");
		System.out.println("Vacia cafetera");
		cafeteraConMaximayActual.vaciarCafetera();
		System.out.println(cafeteraConMaximayActual.get_cantidadActual());
		
		
		
	}

}
